\section{Introduction}
\label{sec:introduction}

The Magnuson-Stevens Fishery Conservation and Management Act~\cite{msfcact}, the framework for fisheries management in the United States, requires that managed fish stocks undergo periodic assessment to determine if they are overfished or are experiencing overfishing. A basic stock assessment requires data on fishery abundance, biology (e.g. age, growth, fecundity), and catch. While demands to continually improve stock assessments are high, the greatest impediment to their accuracy, precision, and credibility remains a lack of adequate input data \cite{deriso1998improving}. Recent developments in low-cost autonomous underwater vehicles (AUVs), stationary camera arrays, and towed vehicles has made it possible for fishery scientists to begin generating species-specific, size-structured abundance estimates for different species of marine organisms from imagery and video. To this end, NOAA Fisheries and other agencies are increasingly employing camera-based surveys to abundance estimation \cite{cappo2006counting}. However, the volume of optical data produced quickly exceeds the capabilities of human analysis. To move into operational use, automated video analysis solutions are needed to extract species-specific, size-structured abundance measures from optical data streams.

This paper presents a cost-effective open-source computer vision software platform for automating the image analysis process. The system provides a common interface for several algorithm subcomponents (stereo matching, object detection, etc.), multiple implementations of each, as well as unified methods for scoring different algorithms for accomplishing the same task. The common open-source framework facilitates the development of additional image analysis modules and pipelines through continuing collaboration within the image analysis and fisheries science communities.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{content/figures/platforms_ex.png}

  \caption{A few examples of devices used to collect imagery underwater, 
  including towed vehicles (top right~\cite{williams2010cam}, bottom right~\cite{gallager2004high})
  and a stationary camera array (left). }
  \label{fig:platforms}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{content/figures/data_ex.png}
  \caption{Example optical data from the devices shown in Figure~\ref{fig:platforms}.}
  \label{fig:data}
\end{figure}

The platform itself can be divided into two core components: image processing elements which fit into a pipelined processing framework, and auxiliary tools which exist outside this streaming framework. Image processing elements were designed to be interchangeably implemented in many of the most popular languages used for computer vision, such as C, C++, Python, and Matlab. This processing architecture will be described further in Section \ref{sec:architecture}. A graphical interface is provided in the framework for visualizing individual object detections, making new annotations, and filtering detections based on classification values. There are two separate scoring tools included, one for generating basic statistics over detections compared with some groundtruth (detection rates, specificity, false alarm rate, etc.) and a second for generating receiver operating characteristic (ROC) curves for detections which contain associated category probabilities. Both the scoring and GUI tools work with either single frame object detections, or spatiotemporal object tracks.
